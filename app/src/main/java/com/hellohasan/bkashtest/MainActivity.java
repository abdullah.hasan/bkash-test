package com.hellohasan.bkashtest;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hellohasan.bkashtest.model.Checkout;
import com.hellohasan.bkashtest.model.PaymentRequest;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    WebView mWebView;
    WebSettings webSettings;
    ProgressBar progressBar;

    private String request = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = findViewById(R.id.webView);
        progressBar = findViewById(R.id.progressBar);

        Checkout checkout = new Checkout();
        checkout.setAmount("15200");
        checkout.setWallet("01770618575");
        checkout.setIntent("sale");

        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(checkout.getAmount());
        paymentRequest.setIntent(checkout.getIntent());

        Gson gson = new Gson();
        request = gson.toJson(paymentRequest);

        webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.setClickable(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(false);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.clearCache(true);
        mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
        mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);

        mWebView.addJavascriptInterface(new JavaScriptInterface(this), "AndroidNative");

        mWebView.loadUrl("file:///android_asset/www/checkout.html");

        mWebView.setWebViewClient(new CheckoutWebViewClient());
    }

    class CheckoutWebViewClient extends WebViewClient {

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.equals("https://www.bkash.com/terms-and-conditions")) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(myIntent);
                return true;
            }
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            String paymentRequest = "{paymentRequest:" + request + "}";
            mWebView.loadUrl("javascript:callReconfigure(" + paymentRequest + " )");
            mWebView.loadUrl("javascript:clickPayButton()");
            progressBar.setVisibility(View.GONE);
        }

    }
}
